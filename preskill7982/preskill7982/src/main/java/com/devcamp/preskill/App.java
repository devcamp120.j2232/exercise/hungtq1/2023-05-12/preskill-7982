package com.devcamp.preskill;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * A very simple program that writes some data to an Excel file
 *
 */
public class App {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();  //create a workbook of excel file
		XSSFSheet sheet = workbook.createSheet("Java Books"); // create a sheet of the workbook
		
		Object[][] bookData = {	//create input data							
				{"Head First Java", "Kathy Serria", 49},
				{"Effective Java", "Joshua Bloch", 50},
				{"Clean Code", "Robert martin", 49},
				{"Thinking in Java", "Bruce Eckel", 50},
		};

		int rowCount = 0;
		
		for (Object[] aBook : bookData) {
			Row row = sheet.createRow(++rowCount); // create rows of the sheet
			
			int columnCount = 0;
			
			for (Object field : aBook) {
				Cell cell = row.createCell(++columnCount); // create cells of a row
				if (field instanceof String) {
					cell.setCellValue((String) field); // set type of the cells' data to String
				} else if (field instanceof Integer) {
					cell.setCellValue((Integer) field); // set type of the cells' data to Integer
				}
			}
			
		}
		
		
		try (FileOutputStream outputStream = new FileOutputStream("JavaBooks.xlsx")) {
			workbook.write(outputStream);
		}
	}

}